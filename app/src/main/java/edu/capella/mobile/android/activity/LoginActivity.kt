package edu.capella.mobile.android.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.util.Pair
import android.view.View
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.Toast
import edu.capella.mobile.android.R
import edu.capella.mobile.android.Utils.Constants.FORGOT_PASSWORD
import edu.capella.mobile.android.Utils.Constants.LEARN_ABOUT_CAPELLA
import edu.capella.mobile.android.Utils.Constants.RESET_PASSWORD
import edu.capella.mobile.android.Utils.DialogUtils
import edu.capella.mobile.android.interfaces.EvenListener
import edu.capella.mobile.android.Utils.Util
import edu.capella.mobile.android.interfaces.NetworkListener
import edu.capella.mobile.android.network.NetworkConstants
import edu.capella.mobile.android.network.NetworkHandler
import edu.capella.mobile.android.base.BaseActivity
import kotlinx.android.synthetic.main.activity_login.*
import java.util.HashMap

/*
 * This is  Login Actiivty
 * Class Description : In this class user can login ,reset password and forgot password
 * @author  :  Kush.pandya
 * @version :  1.0
 * @since   :  10-02-2020
 *
 */
class LoginActivity : BaseActivity(), View.OnClickListener, CompoundButton.OnCheckedChangeListener,
    TextWatcher ,NetworkListener{

    var isLogin: Boolean = false
    var showPassword: Boolean = false
    var showCheckBoxDialog: Boolean = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        attachListener()
    }
    //   set a click listener for variables
    private fun attachListener() {
        checkBox.setOnCheckedChangeListener(this)
        btnLogin.setOnClickListener(this)
        txtPassword.addTextChangedListener(this)
        showPsw.setOnClickListener(this)
        forgotUserTV.setOnClickListener(this)
        resetPswTV.setOnClickListener(this)
        learnAboutTV.setOnClickListener(this)
        tollNumberTxt.setOnClickListener(this)
    }


    private fun doLogin()
    {
        this.isLogin = true
        Log.i("Logged In : ", " = " + this.isLogin)
        startActivity(Intent(this, MenuActivity::class.java))
        finish()

        var usr = (findViewById<EditText>(R.id.txtEmail)).text.toString()
        var psw = (findViewById<EditText>(R.id.txtPassword)).text.toString()
        performLogin(usr , psw)
    }
    //   This method is used to implement functionality of click event

    private fun isLoggedIn() = this.isLogin


    override fun onClick(v: View?)
    {

        when (v!!.id)
        {
            R.id.showPsw ->
            {
                if (!showPassword)
                {
                    showPassword()
                } else
                {
                    hidePassword()
                }
            }

            R.id.forgotUserTV->
            {
                Util.openUrlBroswer(this,FORGOT_PASSWORD)
            }

            R.id.resetPswTV->
            {
                Util.openUrlBroswer(this,RESET_PASSWORD)
            }
            R.id.learnAboutTV->
            {
                Util.openUrlBroswer(this,LEARN_ABOUT_CAPELLA)
            }
            R.id.btnLogin ->
            {
                doLogin()
            }
            R.id.tollNumberTxt ->
            {
                Util.dialNumber(this, tollNumberTxt.text.toString())
            }
        }
    }

//    This method is used to implement functionality of text change listener in password edit text

    override fun afterTextChanged(p0: Editable?) {

        if(p0.toString().isNotEmpty())
        {
            showPsw.visibility=View.VISIBLE
        }
        else{
            showPsw.visibility=View.GONE
        }

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int)
    {
    }



    //   This method is used to show password
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

    private fun showPassword() {
        showPassword = true
        showPsw.text = resources.getText(R.string.hide)
        txtPassword.transformationMethod = HideReturnsTransformationMethod.getInstance();
        txtPassword.setSelection(txtPassword.text.length)
    }


    //   This method is used to handle condition of  keep me signe in
    override fun onCheckedChanged(p0: CompoundButton?, isChecked: Boolean) {
        if (isChecked) {
            if (showCheckBoxDialog) {
                openSignInDialog()
                checkBox.isChecked = false
            }
        } else {
            showCheckBoxDialog = true
        }
    }
    //   This method is used to handle dialog call listener
    inner class ItemEventListener : EvenListener {
        override fun confirm() {
            super.confirm()
            showCheckBoxDialog = false
            checkBox.isChecked = true
        }

    }
    //   This method is used to hide password
    private fun hidePassword() {
        showPassword = false
        showPsw.text = resources.getText(R.string.show)
        txtPassword.transformationMethod = PasswordTransformationMethod.getInstance();
        txtPassword.setSelection(txtPassword.text.length)
    }
    //   This method is used to open signe in dialog
    private fun openSignInDialog() {
        DialogUtils.onKeepInignDialog(this, ItemEventListener())

    }
    //   This method is used to call login api
    private fun performLogin(userName: String , password:String)
    {
        var params  = HashMap<String,String>()
        params.put("username", userName)
        params.put("password", password)

        var networkHandler = NetworkHandler(this, NetworkConstants.LOGIN_API , params ,  NetworkHandler.METHOD_GET , this)

        networkHandler.execute()


    }

    override fun onNetworkResponse(response: Pair<String, String>) {
        Util.trace("Login first :  " + response.first)
        Util.trace("Login second :  " + response.second)
        Toast.makeText(this, response.second , Toast.LENGTH_LONG).show()
    }

}