package edu.capella.mobile.android.network

import android.util.Log
import android.util.Pair
import edu.capella.mobile.android.Utils.Util


import java.io.BufferedInputStream
import java.io.UnsupportedEncodingException
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.util.HashMap

import javax.net.ssl.HttpsURLConnection

/**
 * <h1> NetworkService </h1>
 *
 *  A class to perform synchronous network operation with different HTTP methods like GET / POST / DELETE and JSON Request.
 *
 * Created by Jayesh Lahare on 10/8/16.
 *
 * @author Jayesh Lahare
 * @version 1.0
 * @since 10 /8/16.
 */


object NetworkService {

    /**
     * The constant INTERNAL_NETWORK_ERROR.
     */
    val INTERNAL_NETWORK_ERROR = "Internal Server Error."

    /**
     * Method to call GET method with or without parameters.
     *
     * @param url            the url
     * @param postParameters the parameters required to create query string, can be null.
     * @return the pair,  the response in Pair format.
     * @see Pair
     */
    fun sendHttpGet(url: String, postparameters: HashMap<String, String>?): Pair<String, String> {
        val dataReceived = StringBuffer()
        try {
            var finalUrl = url

            if (postparameters != null)
                finalUrl += getQueryString(postparameters)

            Util.trace("Url ", "Url : $finalUrl")

            val urlLink = URL(finalUrl)

            val connection = urlLink.openConnection() as HttpURLConnection

            //connection.setUseCaches(true);
            connection.readTimeout = 10000
            connection.connectTimeout = 15000
            connection.requestMethod = "GET"

            //conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            //connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-Type", "application/json")

            connection.useCaches = false
            connection.allowUserInteraction = false


            val responseCode = connection.responseCode
            val responseMsg = connection.responseMessage


//            if (responseCode != 200) {
//                return if (responseMsg.isNotEmpty())
//                    Pair.create(NetworkConstants.NETWORK_ERROR, responseMsg)
//                else
//                    Pair.create(NetworkConstants.NETWORK_ERROR, INTERNAL_NETWORK_ERROR)
//
//            }

            Util.trace("Response Code $responseCode")

            var bi = BufferedInputStream(connection.inputStream)

            val buffer = ByteArray(1024)

            var ch: Int
            do
            {

                ch = bi.read(buffer)
                dataReceived.append(String(buffer, 0, ch))
            } while(ch !=-1)


            bi.close()
            connection.disconnect()

            Util.trace("Response", "" + dataReceived.toString())

            return Pair.create(NetworkConstants.NETWORK_SUCCESS, dataReceived.toString())

        } catch (t: Throwable) {
            Util.trace("Error = $t")

            t.printStackTrace()
            return Pair.create(NetworkConstants.NETWORK_ERROR, t.toString())

        }

    }

    /**
     * Method to call POST method with or without parameters.
     *
     * @param urlString            the url
     * @param postParameters the parameters required to post data, can be null.
     * @return the pair,  the response in Pair format.
     * @see Pair
     */
    fun sendHttpPost(
        urlString: String,
        postParameters: HashMap<String, String>?
    ): Pair<String, String> {
        val dataReceived = StringBuffer()

        requireNotNull(postParameters) { "Post parameters can not be null" }

        try {
            val url = URL(urlString)

            Util.trace("Url ", "Url : $url")

            val connection = url.openConnection()

            val conn = url.openConnection() as HttpsURLConnection

            conn.readTimeout = 10000
            conn.connectTimeout = 15000
            conn.requestMethod = "POST"
            //conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Accept", "application/json")

            conn.doInput = true
            conn.doOutput = true

            val writer = conn.outputStream
            //BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

            writer.write(getPostString(postParameters).toByteArray())
            writer.flush()
            writer.close()

            conn.connect()


            val responseCode = conn.responseCode
            val responseMsg = conn.responseMessage


            if (responseCode != 200) {
                return if (responseMsg.isNotEmpty())
                    Pair.create(NetworkConstants.NETWORK_ERROR, responseMsg)
                else
                    Pair.create(NetworkConstants.NETWORK_ERROR, INTERNAL_NETWORK_ERROR)

            }

            Util.trace("Response Code $responseCode")

            var bi = BufferedInputStream(conn.inputStream)

            val buffer = ByteArray(1024)

            var ch: Int
            do
            {

                ch = bi.read(buffer)
                dataReceived.append(String(buffer, 0, ch))
            } while(ch !=-1)


            bi.close()
            conn.disconnect()

            Util.trace("Response Received", " = $dataReceived")

            return Pair.create(NetworkConstants.NETWORK_SUCCESS, dataReceived.toString())


        } catch (t: Throwable) {
            Log.e("Error ", " = $t")
            t.printStackTrace()
            return Pair.create(NetworkConstants.NETWORK_ERROR, t.toString())
        }

    }


    @Throws(UnsupportedEncodingException::class)
    private fun getPostString(parameters: HashMap<String, String>?): String {
        val result = StringBuilder()

        if (parameters != null) {
            var i = 0

            for ((key, value) in parameters) {

                if (i == 0) {
                    i++

                } else {
                    result.append("&")
                }

                result.append(URLEncoder.encode(key, "UTF-8"))
                result.append("=")
                result.append(URLEncoder.encode(value, "UTF-8"))

                /*result.append(key);
                result.append("=");
                result.append(value);*/
            }
        }


        return result.toString()
    }


    @Throws(UnsupportedEncodingException::class)
    private fun getQueryString(parameters: HashMap<String, String>?): String {
        val result = StringBuilder()

        if (parameters != null) {

            //Set keys = this.parameters.keySet();

            var i = 0

            for ((key, value) in parameters) {

                if (i == 0) {
                    result.append("?$key=$value")
                    i++
                } else {
                    result.append("&$key=$value")
                }
            }
        }
        return result.toString()
    }


    /**
     * Method to call url with JSON request, with or without parameters.
     *
     * @param urlString  the url
     * @param jsonData   the JSON string as a request, can be null.
     * @return the pair,  the response in Pair format.
     * @see Pair
     */
    fun sendHttpPostJSON(urlString: String, jsonData: String?): Pair<String, String> {
        val dataReceived = StringBuffer()

        requireNotNull(jsonData) { "Json data can not be null" }

        //throw new Exception("Post parameters can not be null");

        try {

            val url = URL(urlString)

            Util.trace("Url ", "Url : $url")


            val conn = url.openConnection() as HttpURLConnection

            conn.readTimeout = 10000
            conn.connectTimeout = 15000
            conn.requestMethod = "POST"
            //conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            //conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json")

            // conn.setRequestProperty("Connection" , "keep-alive");
            //conn.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");

            conn.doInput = true
            conn.doOutput = true

            val writer = conn.outputStream

            writer.write(jsonData.toByteArray())
            writer.flush()
            writer.close()

            conn.connect()

            val responseCode = conn.responseCode
            val responseMsg = conn.responseMessage


            if (responseCode != 200) {
                return if (responseMsg.isNotEmpty())
                    Pair.create(NetworkConstants.NETWORK_ERROR, responseMsg)
                else
                    Pair.create(NetworkConstants.NETWORK_ERROR, INTERNAL_NETWORK_ERROR)

            }


            Util.trace("Response Code $responseCode")

            val bi = BufferedInputStream(conn.inputStream)

            val buffer = ByteArray(1024)
            var ch: Int
            do
            {
                ch = bi.read(buffer)
                dataReceived.append(String(buffer, 0, ch))
            } while(ch !=-1)

            Util.trace("Received", "" + dataReceived.toString())

            bi.close()
            conn.disconnect()

            return Pair.create(NetworkConstants.NETWORK_SUCCESS, dataReceived.toString())

        } catch (t: Throwable) {
            Util.trace("Error ", " = $t")
            t.printStackTrace()
            return Pair.create(NetworkConstants.NETWORK_ERROR, t.toString())
        }

    }


    /**
     * Method to call DELETE method, with or without parameters.
     *
     * @param url  the url
     * @param deleteParameters   the parameters need to send, can be null.
     * @return the pair,  the response in Pair format.
     * @see Pair
     */
    fun sendHttpDelete(
        url: String,
        deleteParameters: HashMap<String, String>?
    ): Pair<String, String> {
        val dataReceived = StringBuffer()
        try {
            var finalUrl = url

            if (deleteParameters != null)
                finalUrl += getQueryString(deleteParameters)

            Util.trace("Url ", "Url : $finalUrl")

            val urlLink = URL(finalUrl)

            val connection = urlLink.openConnection() as HttpURLConnection

            //connection.setUseCaches(true);
            connection.readTimeout = 10000
            connection.connectTimeout = 15000
            connection.requestMethod = "DELETE"

            //conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            //connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-Type", "application/json")

            connection.useCaches = false
            connection.allowUserInteraction = false


            val responseCode = connection.responseCode
            val responseMsg = connection.responseMessage


            if (responseCode != 200) {
                return if (responseMsg.isNotEmpty())
                    Pair.create(NetworkConstants.NETWORK_ERROR, responseMsg)
                else
                    Pair.create(NetworkConstants.NETWORK_ERROR, INTERNAL_NETWORK_ERROR)

            }

            Util.trace("Response Code $responseCode")

            val bi = BufferedInputStream(connection.inputStream)

            val buffer = ByteArray(1024)
            var ch: Int
            do
            {

                ch = bi.read(buffer)
                dataReceived.append(String(buffer, 0, ch))
            } while(ch !=-1)

            bi.close()
            connection.disconnect()

            Util.trace("Response", "" + dataReceived.toString())

            return Pair.create(NetworkConstants.NETWORK_SUCCESS, dataReceived.toString())

        } catch (t: Throwable) {
            Util.trace("Error = $t")

            t.printStackTrace()
            return Pair.create(NetworkConstants.NETWORK_ERROR, t.toString())

        }

    }

}
