package edu.capella.mobile.android.Utils

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.util.Log

/**
 * <h1> Util </h1>
 *
 *  A Util class to provide certain common utility functions.
 *
 *
 * Created by Jayesh Lahare on 10/8/16.
 *
 * @author Jayesh Lahare
 * @version 1.0
 * @since 10/8/16.
 */
object Util {
    /**
     * Shows LOG with tag and message.
     *
     * @param tag     the tag
     * @param message the message
     */
    fun trace(tag: String?, message: String?) {
        Log.e(tag, message)
    }

    /**
     * Shows LOG without tag
     *
     * @param message the message
     */
    fun trace(message: String?) {
        Log.e("General", message)
    }

    /**
     * Checks whether internet is available on phone or not.
     *
     * @param context the context
     * @return the boolean
     */
    fun isInternetAvailable(context: Context): Boolean {
        val connectivity = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivity != null) {
            val info = connectivity.allNetworkInfo
            if (info != null) for (i in info.indices) if (info[i].state == NetworkInfo.State.CONNECTED) {
                trace(
                    "Network",
                    "NETWORKnAME: " + info[i].typeName
                )
                return true
            }
        }
        return false
    }

    fun openUrlBroswer(context: Context , url: String)
    {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        context.startActivity(browserIntent)
    }

    fun dialNumber(context: Context , numTODial: String)
    {
        try
        {
            val dialIntent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + Uri.encode(numTODial)))
            context.startActivity(dialIntent)

        }catch (th : Throwable)
        {
            // TO-DO
        }
    }
}