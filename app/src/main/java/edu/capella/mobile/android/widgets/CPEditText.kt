package edu.capella.mobile.android.widgets

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.EditText


/**
 * <h1> CPEditText </h1>
 *
 *  Custom EditText class with specific font with normal and bold style.
 *
 *
 * Created by Kush Pandya on 20/7/16.
 *
 * @author Kush Pandya
 * @version 1.0
 * @since 30/1/2020.
 * @see android.widget.EditText

 */
class CPEditText : EditText {

    /**
     * Instantiates a new Biz edit text.
     *
     * @param context the context
     */
    constructor(context: Context) : super(context) {
        applyFont(context)
    }


    /**
     * Instantiates a new Biz edit text.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        applyFont(context)
    }

    /**
     * Instantiates a new Biz edit text.
     *
     * @param context      the context
     * @param attrs        the attrs
     * @param defStyleAttr the def style attr
     */
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        applyFont(context)

    }

    /**
     * Internal method to appy font setting over EditText.
     */
    private fun applyFont(context: Context) {
        try {
            //Typeface typeFace = Typeface.createFromAsset(context.getAssets(),"fonts/alex_brush.ttf");
            //Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/roboto_regular.ttf");
            val typeFace = Typeface.createFromAsset(context.assets, "fonts/helvetica.ttf")
            this.typeface = typeFace


        } catch (t: Throwable) {
        }

    }

}
