package edu.capella.mobile.android.interfaces

import android.util.Pair

/**
 * <h1> NetworkListener </h1>
 *
 *  A network interface used as a callback or listener for different network activities.
 *
 * Created by Jayesh Lahare on 10/8/16.
 *
 * @author Jayesh Lahare
 * @version 1.0
 * @since 10 /8/16.
 */
interface NetworkListener {
    /**
     * A callback method, executes when Network Handler class completes its network operations
     * used to inform activity about server response, whether its error or json data receievd from
     * server.
     *
     * @see edu.capella.mobile.android.network.NetworkHandler
     *
     *
     * @param response the response, an object of Pair class.
     */
    fun onNetworkResponse(response: Pair<String, String>)

}
