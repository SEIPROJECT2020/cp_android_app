package edu.capella.mobile.android.widgets


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.TextView
import android.widget.Toast
import edu.capella.mobile.android.R


/**
 * <h1> CPToast </h1>
 *
 *  Class CPToast used to show custom TOAST messages in app with different color combinations.
 * Created by Jayesh Lahare on 1/30/20.
 *
 * @author Jayesh Lahare
 * @version 1.0
 * @since 1/30/20.
 */
object CPToast {

    /**
     * Shows custom toast message with default_icon green color theme.
     *
     * @param context the context for which toast needs to show.
     * @param message the message which needs to be display.
     * @param length  the length of time duration, short or long.
     */
    fun show(context: Context, message: String, length: Int) {
        val li = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout = li.inflate(R.layout.toast_layout, null)
        (layout.findViewById(R.id.toastTxt) as TextView).text = message
        //Creating the Toast object
        val toast = Toast(context)
        toast.duration = length
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
        toast.view = layout
        toast.show()
    }


    /**
     * Shows custom toast message with red color theme to show error message.
     *
     * @param context the context for which toast needs to show.
     * @param message the message which needs to be display.
     * @param length  the length of time duration, short or long.
     */
    @SuppressLint("InflateParams")
    fun showError(context: Context, message: String, length: Int) {
        val li = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val layout = li.inflate(R.layout.toast_layout, null)


        val tv = layout.findViewById(R.id.toastTxt) as TextView
        tv.text = message
        tv.setBackgroundResource(R.drawable.white_filled_rounded_error)

        //Creating the Toast object
        val toast = Toast(context)
        toast.duration = length
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
        toast.view = layout
        toast.show()
    }


    /**
     * Shows custom toast message with yellow color theme to show information.
     *
     * @param context the context for which toast needs to show.
     * @param message the message which needs to be display.
     * @param length  the length of time duration, short or long.
     */
    @SuppressLint("InflateParams")
    fun showInformation(context: Context, message: String, length: Int) {
        val li = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val layout = li.inflate(R.layout.toast_layout, null)

        val tv = layout.findViewById(R.id.toastTxt) as TextView
        tv.text = message
        tv.setBackgroundResource(R.drawable.green_filled_rounded_information)

        //Creating the Toast object
        val toast = Toast(context)
        toast.duration = length
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
        toast.view = layout
        toast.show()
    }

    /**
     * Shows custom temporary toast message with under development message.
     *
     * @param context the context for which toast needs to show.
     */
    fun showDevelopmentMessage(context: Context) {
        val li = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout = li.inflate(R.layout.toast_layout, null)

        val tv = layout.findViewById(R.id.toastTxt) as TextView
        tv.text = "Functionality under development."

        //tv.setBackgroundResource(R.drawable.green_filled_rounded_information);
        tv.setBackgroundResource(R.drawable.yellow_filled_rounded_information)
        tv.setTextColor(Color.WHITE)

        //Creating the Toast object
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_SHORT
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
        toast.view = layout
        toast.show()
    }


}
