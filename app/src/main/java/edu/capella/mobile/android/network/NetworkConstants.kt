package edu.capella.mobile.android.network

import edu.capella.mobile.android.BuildConfig


/**
 * <h1> NetworkConstants </h1>
 *
 *  A class contains constant parameters for different webservices
 *
 * Created by Jayesh Lahare on 24/10/16.
 *
 * @author Jayesh Lahare
 * @version 1.0
 * @see NetworkService
 *
 * @since 24/10/16.
 */

object NetworkConstants {


    //=======Network Constants==============
    val NETWORK_SUCCESS = "success"
    /**
     * The constant NETWORK_ERROR.
     */
    val NETWORK_ERROR = "error"
    /**
     * The constant NETWORK_INTERNET_ERROR.
     */
    val NETWORK_INTERNET_ERROR = "internet_error"

    /**
     * The constant SUCCESS.
     */
    val SUCCESS = "success"
    /**
     * The constant ERROR.
     */
    val ERROR = "error"

    //=======API's==============

    var LOGIN_API = BuildConfig.HOST + "/mobile-feed/json/login.feed"



}
