package edu.capella.mobile.android.Utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.widget.TextView
import edu.capella.mobile.android.R
import edu.capella.mobile.android.interfaces.EvenListener

/*
 * <H1>Class Name</H1>
 * <b>Class Description :</b> class description goes here
 *
 * @author  :  Kush.pandya
 * @version :  1.0
 * @since   :  10-02-2020
 *
 * @param param1 <p>Constructor Param1 Description Goes here</p>
 * @param param2 <p>Constructor Param2 Description Goes here</p>
 *
 */object DialogUtils {

    fun onKeepInignDialog(mcontext: Context,Listener: EvenListener) {
        try {
            val dialog = Dialog(mcontext,R.style.DialogTheme)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setCanceledOnTouchOutside(false)
            dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setContentView(R.layout.keep_me_signed_layout)
            val cancelTV = dialog.findViewById(R.id.cancelTV) as TextView
            val comfirmTV = dialog.findViewById(R.id.comfirmTV) as TextView

            comfirmTV.setOnClickListener {
                dialog.dismiss()
                Listener.confirm()
            }
            cancelTV.setOnClickListener { dialog.dismiss() }
            dialog.show()
        }catch (e: Exception) {
            e.printStackTrace()
        }
    }

}