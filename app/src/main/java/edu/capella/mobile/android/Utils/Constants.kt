package edu.capella.mobile.android.Utils

/*
 * <H1>Class Name</H1>
 * <b>Class Description :</b> class description goes here
 *
 * @author  :  jayesh.lahare
 * @version :  1.0
 * @since   :  2/10/2020
 *
 * @param param1 <p>Constructor Param1 Description Goes here</p>
 * @param param2 <p>Constructor Param2 Description Goes here</p>
 *
 */

object Constants
{
    val APP_TIMEOUT_SECONDS: Long = 10000 //10 Seconds
    val RESET_PASSWORD:String="https://www.capella.edu/enterpriseforms/forgotpassword/"
    val FORGOT_PASSWORD:String="https://www.capella.edu/enterpriseforms/forgotusername"
    val LEARN_ABOUT_CAPELLA:String="https://www.capella.edu"
}