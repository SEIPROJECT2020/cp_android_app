package edu.capella.mobile.android.network

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.AsyncTask
import android.util.Pair
import edu.capella.mobile.android.R

import edu.capella.mobile.android.Utils.Util

import edu.capella.mobile.android.interfaces.NetworkListener

import java.util.HashMap

/**
 * <h1> NetworkHandler </h1>
 *
 *  A asynchronous operations are performed over network operation with GET and POST methods over synchronous calls.
 * synchronous call for network is made by **NetworkService**
 *
 * Created by Jayesh Lahare on 10/8/16.
 *
 * @author Jayesh Lahare
 * @version 1.0
 * @see NetworkService
 *
 * @since 10 /8/16.
 */
class NetworkHandler : AsyncTask<String, String, Pair<String, String>> {

    @SuppressLint("StaticFieldLeak")
    private var context: Context
    private var url: String? = null
    private var paramaters: HashMap<String, String>? = null

    private var jsonData: String? = null
    private var methodType: Short = 0

    private var listener: NetworkListener? = null
    private var progressBar: ProgressDialog? = null


    /**
     * Instantiates a new Network handler, This constructor do not have Network Listener feature.
     * but supports GET, POST & DELETE http Methods
     *
     * @param context    : Context of application of activity for which network operation is going to                     perform. batter use application context to avoid context leak isuse.
     * @param url        : A Url link for which network operation is going to perform.
     * @param paramaters : Input parameters for GET or POST method.
     * @param methodType : Method type GET or POST.
     */
    constructor(
        context: Context,
        url: String,
        paramaters: HashMap<String, String>,
        methodType: Short
    ) {
        this.url = url
        this.paramaters = paramaters
        this.context = context
        this.methodType = methodType

        require(!(methodType != METHOD_GET && methodType != METHOD_POST && methodType != METHOD_DELETE)) {
            context.resources.getString(
                R.string.method_required
            )
        }
    }


    /**
     * Instantiates a new Network handler., This constructor have Network Listener feature.
     * and supports GET, POST & DELETE http Methods
     *
     * @param context         : Context of application of activity for which network operation is going to                     perform. batter use application context to avoid context leak isuse.
     * @param url             : A Url link for which network operation is going to perform.
     * @param paramaters      : Input parameters for GET or POST method.
     * @param methodType      : Method type GET or POST.
     * @param networkListener : A network listener interface used a callback for network response.
     * @see NetworkListener
     */
    constructor(
        context: Context,
        url: String,
        paramaters: HashMap<String, String>,
        methodType: Short,
        networkListener: NetworkListener
    ) {
        this.url = url
        this.paramaters = paramaters
        this.context = context
        this.listener = networkListener
        this.methodType = methodType

        require(!(methodType != METHOD_GET && methodType != METHOD_POST && methodType != METHOD_DELETE)) {
            context.resources.getString(
                R.string.method_required
            )
        }
    }


    /**
     * Instantiates a new Network handler, This constructor supports INPUT type as JSON and have
     * Network Listener feature, it supports only POST http Methods
     *
     * @param context         the context
     * @param url             the url
     * @param jsonData        the json data
     * @param networkListener the network listener
     */
    constructor(context: Context, url: String, jsonData: String, networkListener: NetworkListener) {
        this.url = url
        this.jsonData = jsonData
        this.context = context
        this.listener = networkListener
        this.methodType = METHOD_POST_JSON
    }


    private fun initView() {
        try {
            if (progressBar == null) {
                progressBar = ProgressDialog(context)
                progressBar!!.show()
                progressBar!!.setCancelable(false)
                progressBar!!.setContentView(R.layout.network_progress)
                progressBar!!.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

            } else
                progressBar!!.show()

        } catch (t: Throwable) {
            Util.trace("ProgressBar", "Progress Init Issue $t")
        }

    }


    /**
     * Sets network listener.
     *
     * @param listener : A network listener interface used a callback for network response.
     * @see NetworkListener
     */
    fun setNetworkListener(listener: NetworkListener) {
        this.listener = listener
    }


    /**
     * On pre execute.
     */
    override fun onPreExecute() {
        super.onPreExecute()
        initView()
    }

    /**
     * Do in background pair.
     *
     * @param strings the strings
     * @return the pair as response.
     */
    override fun doInBackground(vararg strings: String): Pair<String, String>? {
        var pair: Pair<String, String>? = null

        try {
            Thread.sleep(200)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }


        if (Util.isInternetAvailable(context)) {
            when {
                this.methodType == METHOD_GET -> pair = NetworkService.sendHttpGet(this.url!!, this.paramaters)
                methodType == METHOD_POST -> pair = NetworkService.sendHttpPost(this.url!!, this.paramaters)
                this.methodType == METHOD_POST_JSON -> pair = NetworkService.sendHttpPostJSON(this.url!!, jsonData)
                this.methodType == METHOD_DELETE -> pair = NetworkService.sendHttpDelete(this.url!!, this.paramaters)
            }
        } else {
            pair = Pair.create(
                NetworkConstants.NETWORK_INTERNET_ERROR,
                "Please connect with internet first."
            )
        }

        return pair
    }

    /**
     * On post execute.
     *
     * @param pair the pair as response.
     */
    override fun onPostExecute(pair: Pair<String, String>) {
        try {
            if (progressBar != null)
                progressBar!!.dismiss()
        } catch (t: Throwable) {
            Util.trace("ProgressBar", "Progress Hide Issue $t")
        }

        if (listener != null)
            listener!!.onNetworkResponse(pair)
    }

    companion object {

        /**
         * The constant METHOD_GET.
         */
        var METHOD_GET: Short = 1
        /**
         * The constant METHOD_POST.
         */
        var METHOD_POST: Short = 2
        /**
         * The constant METHOD_POST_JSON.
         */
        var METHOD_POST_JSON: Short = 3
        /**
         * The constant METHOD_DELETE.
         */
        var METHOD_DELETE: Short = 4
    }


}
