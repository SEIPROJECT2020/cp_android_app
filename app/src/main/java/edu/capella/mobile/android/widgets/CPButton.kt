package edu.capella.mobile.android.widgets


import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.Button

/**
 * <h1> CPButton </h1>
 *
 *  Custom button class with specific font.
 *
 *
 * Created by Kush Pandya on 20/7/16.
 *
 * @author Kush Pandya
 * @version 1.0
 * @since 30/1/2020.
 * @see android.widget.Button
 *
 */

class CPButton : Button {

    /**
     * Instantiates a new Biz button.
     *
     * @param context the context
     */
    constructor(context: Context) : super(context) {
        applyFont(context, -1)
    }

    /*
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        applyFont(getContext());

    }*/

    /**
     * Instantiates a new Biz button.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        appySettings(context, attrs)
    }

    private fun appySettings(context: Context, attrs: AttributeSet) {

        val set = intArrayOf(
            android.R.attr.textStyle //, android.R.attr.text
        )

        val a = context.obtainStyledAttributes(attrs, set)
        val style = a.getInt(0, 0)
        applyFont(context, style)
    }

    /**
     * Instantiates a new Biz button.
     *
     * @param context      the context
     * @param attrs        the attrs
     * @param defStyleAttr the def style attr
     */
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        appySettings(context, attrs)

    }

    /**
     * Internal method to apply font relevant settings to Button.
     *
     */
    private fun applyFont(context: Context, style: Int) {
        try {
            var typeFace: Typeface? = null

            if (style == Typeface.BOLD) {
                typeFace = Typeface.createFromAsset(context.assets, "fonts/roboto_bold.ttf")
            } else if (style == Typeface.NORMAL) {
                typeFace = Typeface.createFromAsset(context.assets, "fonts/roboto_medium.ttf")
            } else {
                typeFace = Typeface.createFromAsset(context.assets, "fonts/roboto_regular.ttf")
            }
            this.typeface = typeFace
        } catch (t: Throwable) {
        }

    }
}
