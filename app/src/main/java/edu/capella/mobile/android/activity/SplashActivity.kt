package edu.capella.mobile.android.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import edu.capella.mobile.android.R
import edu.capella.mobile.android.base.BaseActivity


class SplashActivity : BaseActivity() {

    private lateinit var mHandler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash)
        init()


    }

    //   Initialize value of variable
    private fun init() {
        mHandler = Handler()
        mHandler.postDelayed({
            // This method will be executed once the timer is over
            val i = Intent(this@SplashActivity, LoginActivity::class.java)
            startActivity(i)
            finish()
        }, 2000)

    }

    //   remove callback of handler
    override fun onPause() {
        super.onPause()
        mHandler.removeCallbacksAndMessages(null)
    }

}