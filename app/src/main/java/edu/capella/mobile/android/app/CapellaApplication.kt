package edu.capella.mobile.android.app

import android.app.Application
import android.util.Log
import edu.capella.mobile.android.Utils.AppLifecycleHandler
import edu.capella.mobile.android.Utils.Constants
import edu.capella.mobile.android.Utils.LifeCycleDelegate
import java.util.*



class CapellaApplication : Application(), LifeCycleDelegate {

    val TAG = "Capella"
    var appTimer = Timer()
    lateinit var lifeCycleHandler:AppLifecycleHandler


    override fun onCreate()
    {
        super.onCreate()
        lifeCycleHandler = AppLifecycleHandler(this)
        registerLifecycleHandler(lifeCycleHandler)
    }

    override fun onAppBackgrounded() {


        Log.d(TAG, "App in background")
        resetAppTimer()
    }

    override fun onAppForegrounded() {
        Log.d(TAG, "App in foreground")
        clearAppTimer()
    }

    private fun registerLifecycleHandler(lifeCycleHandler: AppLifecycleHandler) {
        registerActivityLifecycleCallbacks(lifeCycleHandler)
        registerComponentCallbacks(lifeCycleHandler)
    }

    fun resetAppTimer()
    {
        if(appTimer != null)
        {
            appTimer.cancel()
        }


        appTimer  =  Timer()
        var task =  Task(lifeCycleHandler)

        appTimer.schedule(task , Constants.APP_TIMEOUT_SECONDS)

        //Log.e("Capella" , "Timer Started")

    }
    fun clearAppTimer()
    {
        if(appTimer != null)
        {
            appTimer.cancel()
        }
       // Log.e("Capella" , "Timer Cleared")
    }



    class Task(life:AppLifecycleHandler) : TimerTask()
    {
       var lifeVar : AppLifecycleHandler = life

        override fun run()
        {
           // Log.e("Capella" , "Timing Out App......")
            this.lifeVar.killCurrentActivity()

        }

    }
}
