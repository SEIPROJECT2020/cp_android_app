package edu.capella.mobile.android.interfaces

/*
 * <H1>Class Name</H1>
 * <b>Class Description :</b> class description goes here
 *
 * @author  :  Kush.pandya
 * @version :  1.0
 * @since   :  10-02-2020
 *
 * @param param1 <p>Constructor Param1 Description Goes here</p>
 * @param param2 <p>Constructor Param2 Description Goes here</p>
 *
 */interface EvenListener {

    fun cancel()
    {

    }

    fun confirm()
    {

    }
}