package edu.capella.mobile.android

import android.view.View
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import edu.capella.mobile.android.activity.LoginActivity
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import android.widget.EditText
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher



@RunWith(AndroidJUnit4::class)
class LoginActivityTest {

    val currentPkg = "edu.capella.mobile.android"

    //Initialize activity
    @Rule
    @JvmField
    var activityRule: ActivityTestRule<LoginActivity> = ActivityTestRule(LoginActivity::class.java)

    @Test
    fun checkPackageName() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        Assert.assertEquals(currentPkg, appContext.packageName)
    }

    @Test
    fun checkLoginScreenComponents() {
        Espresso.onView(ViewMatchers.withId(R.id.lblEmail))
            .check(ViewAssertions.matches(ViewMatchers.withText("Enter Email :")));
        Espresso.onView(ViewMatchers.withId(R.id.lblPassword))
            .check(ViewAssertions.matches(ViewMatchers.withText("Enter Password :")));
        Espresso.onView(ViewMatchers.withId(R.id.btnLogin))
            .check(ViewAssertions.matches(ViewMatchers.withText("Log In")));

    }

    @Test
    fun checkWhetherItsAcceptingEmailAndPassword() {
        Espresso.onView(ViewMatchers.withId(R.id.txtEmail)).perform(
            ViewActions.typeText("jayesh.lahare@yash.com"),
            ViewActions.closeSoftKeyboard()
        );
        Espresso.onView(ViewMatchers.withId(R.id.txtPassword))
            .perform(ViewActions.typeText("myPassword"), ViewActions.closeSoftKeyboard());
        Espresso.onView(ViewMatchers.withId(R.id.btnLogin)).perform(ViewActions.click());

        Assert.assertEquals(true, activityRule.activity.isLoggedIn());

    }
    @Test
    fun checkWhetherKeepMeSignAvailable() {
        Espresso.onView(withId(R.id.checkBox)).perform(click());
    }

    @Test
    fun checkWhetherEmailIsCorrect() {

        Espresso.onView(ViewMatchers.withId(R.id.txtEmail)).perform(
            ViewActions.typeText("jayesh.lahare@yash.com"),
            ViewActions.closeSoftKeyboard()
        );
        onView(withId(R.id.txtEmail)).check(matches((isEditTextValueEqualTo(R.id.txtEmail, "@"))));

    }

    fun isEditTextValueEqualTo(viewId1: Int, content: String): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun matchesSafely(item: View?): Boolean {
               if( (item!! as EditText ).text.toString().contains("@"))
               {
                   return  true
               }
                else{
                   return  false
               }
            }

            override fun describeTo(description: org.hamcrest.Description?) {
                description!!.appendText("Match Edit Text Value with View ID Value : :  $content")
            }

        }
    }
}
